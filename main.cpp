// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>

int main(int argc, char *argv[])
{
	QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
	app->setApplicationName("boggle.utsav");

	QQuickView *view = new QQuickView();
	view->setSource(QUrl(QStringLiteral("qml/Main.qml")));
	view->setResizeMode(QQuickView::SizeRootObjectToView);
	view->show();

	QObject::connect(
		(QObject*) view->engine(),
		SIGNAL(quit()),
		app,
		SLOT(quit())
	);

	return app->exec();
}

