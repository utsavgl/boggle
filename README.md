# Boggle

Boggle brings [Hasbro's Boggle](https://en.wikipedia.org/wiki/Boggle) to Ubuntu Touch.

## How to play

To begin, hit the *Start* button in the upper right-hand corner. The letter cubes will be "shaken" and displayed, and the countdown timer will commence at three minutes:

![Boggle](assets/sample.svg)

### Adding words

To select a word, touch its first letter and swipe through the remaining letters. For example, to select ZING, touch Z and swipe through I, N and G.

Upon completing the swipe, the last letter will show a green plus sign. Hit that letter to add the word to your list.

The first letter will show a red trash can. If you want to punt the selected word, hit that letter and the selection will be removed.

If the selected word is already present in your word list, it will appear in red in the list and no plus sign will show. The same holds for words that have fewer than three letters. Your only option in those cases is to hit the first letter, remove the word, and continue playing.

### Pausing the game

At any time, you may pause the game by hitting the timer button. To resume it, hit the button again.

### Ending the game

Once time runs out, the game ends. You may end the game early by pressing and holding the timer button.

At the end of the game, the app reveals its words in its list. Words in common are shown crossed out.

To reset the game, hit the timer button: *Start* will reappear and may be hit once more to start a new game.

## Quitting the app

To quit, press and hold the "Boggle" header (upper-left corner).
