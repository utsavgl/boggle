// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.12

MainView {
	applicationName: "boggle.utsav"
	id: main

	width: Math.min(boardView.width * 1.4, Screen.width)
	height: Math.min(boardView.height * 2.4, Screen.height)

	property Board board: Board {}
	property Hourglass hourglass: Hourglass {}
	property MyWordList myWords: MyWordList {}
	property YourWordList yourWords: YourWordList {}
	readonly property Dictionary dictionary: Dictionary {}

	Page {
		anchors.fill: parent
		header: PageHeader {
			id: header
			StyleHints {
				backgroundColor: theme.palette.normal.base
			}
			contents: RowLayout {
				anchors.fill: parent
				Label {
					text: "Boggle"
					textSize: Label.XLarge
					font.italic: true
					font.weight: Font.DemiBold
					color: theme.palette.normal.baseText
					MouseArea {
						anchors.fill: parent
						onPressAndHold: Qt.quit()
					}
				}
				GameButton {
					board: main.board
					hourglass: main.hourglass
					Layout.alignment: Qt.AlignRight
				}
			}
		}

		Rectangle {
			anchors {
				left: parent.left
				right: parent.right
				top: header.bottom
				bottom: parent.bottom
			}
			color: theme.palette.normal.base

			BoardView {
				id: boardView
				board: main.board
				yourWords: main.yourWords
				anchors {
					horizontalCenter: parent.horizontalCenter
					top: parent.top
					topMargin: units.gu(2)
				}
			}

			Row {
				anchors {
					horizontalCenter: parent.horizontalCenter
					top: boardView.bottom
					topMargin: units.gu(2)
					bottomMargin: units.gu(3)
					bottom: parent.bottom
				}
				spacing: units.gu(2)
				readonly property int wordListViewWidth: units.gu(16)
				WordListView {
					width: parent.wordListViewWidth
					title: "Me"
					wordList: myWords
				}
				WordListView {
					width: parent.wordListViewWidth
					title: "You"
					wordList: yourWords
					incremental: true
				}
			}
		}
	}
}

