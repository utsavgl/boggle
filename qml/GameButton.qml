// Copyright 2021 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

Button {
	id: timeButton

	// required
	property Board board

	// required
	property Hourglass hourglass

	state: "start"
	states: [
		State {
			name: "start"
			PropertyChanges {
				target: timeLeftDisplay
				text: "Start"
			}
			PropertyChanges {
				target: timeButton
				onClicked: {
					board.shake()
					state = "running"
				}
			}
		},
		State {
			name: "running"
			StateChangeScript {
				script: hourglass.start()
			}
			PropertyChanges {
				target: boardView
				enabled: true
			}
			PropertyChanges {
				target: timeButton
				onClicked: state = "paused"
			}
		},
		State {
			name: "paused"
			StateChangeScript {
				script: hourglass.stop()
			}
			PropertyChanges {
				target: timeButton
				color: theme.palette.normal.selection
				onClicked: state = "running"
			}
		},
		State {
			name: "ended"
			when: !hourglass.hasTimeLeft
			StateChangeScript {
				script: {
					if (board.firstSelection !== undefined) {
						yourWords.clearWord()
						board.clear()
					}
					board.findWords(myWords)
					myWords.cancelCommon(yourWords)
					yourWords.cancelCommon(myWords)
				}
			}
			PropertyChanges {
				target: timeButton
				color: theme.palette.normal.negative
				onClicked: {
					yourWords.reset()
					myWords.reset()
					board.reset()
					hourglass.reset()
					state = "start"
				}
			}
		}
	]
	color: theme.palette.normal.positive
	Label {
		id: timeLeftDisplay
		anchors.centerIn: parent
		text: hourglass.formattedTime
		textSize: Label.Large
		font.italic: true
		color: "#ffffff"
	}
	onPressAndHold: hourglass.empty()
}

