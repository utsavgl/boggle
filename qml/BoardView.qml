// Copyright 2021 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

UbuntuShape {
	id: boardView

	// required
	property Board board

	// required
	property YourWordList yourWords

	readonly property int buttonSize: units.gu(8)
	readonly property int cellMargin: units.gu(0.4)
	readonly property int cellSize: buttonSize + 2 * cellMargin
	readonly property int margin: cellMargin

	width: grid.width + 2 * margin
	height: grid.height + 2 * margin
	backgroundColor: UbuntuColors.orange
	enabled: false

	Component {
		id: cell
		UbuntuShape {
			width: boardView.cellSize
			height: boardView.cellSize
			backgroundColor: UbuntuColors.orange
			UbuntuShape {
				width: boardView.buttonSize
				height: boardView.buttonSize
				anchors.centerIn: parent
				backgroundColor: "white"
				Button {
					id: letterButton
					width: parent.width
					height: parent.height
					states: [
						State {
							when: (
								board.wordSelected &&
								index === board.firstSelection
							)
							PropertyChanges {
								target: letterButton
								onClicked: {
									yourWords.clearWord()
									board.clear()
								}
							}
							PropertyChanges {
								target: clearWordIcon
								visible: true
							}
						},
						State {
							when: (
								board.wordSelected &&
								index === board.lastSelection &&
								yourWords.wordEndable
							)
							PropertyChanges {
								target: letterButton
								onClicked: {
									yourWords.endWord()
									board.clear()
								}
							}
							PropertyChanges {
								target: endWordIcon
								visible: true
							}
						}
					]
					color: (
						selected ?
						theme.palette.normal.selection :
						theme.palette.normal.foreground
					)
					Label {
						anchors.centerIn: parent
						text: letter
						textSize: Label.XLarge
						font.bold: true
					}
					Icon {
						id: clearWordIcon
						anchors {
							left: parent.left
							top: parent.top
							margins: units.gu(0.5)
						}
						width: units.gu(2)
						height: width
						visible: false
						name: "delete"
						color: theme.palette.normal.negative
					}
					Icon {
						id: endWordIcon
						anchors {
							left: parent.left
							top: parent.top
							margins: units.gu(0.5)
						}
						width: units.gu(2)
						height: width
						visible: false
						name: "add"
						color: theme.palette.normal.positive
					}
				}
			}
		}
	}

	GridView {
		id: grid
		anchors.centerIn: parent
		width: board.size * cellWidth
		height: board.size * cellHeight
		cellWidth: boardView.cellSize
		cellHeight: boardView.cellSize
		interactive: false
		model: board
		delegate: cell
		MouseArea {
			anchors.fill: parent
			enabled: !board.wordSelected
			readonly property alias cellWidth: grid.cellWidth
			readonly property alias cellHeight: grid.cellHeight
			readonly property int senseWidth: units.gu(6)
			readonly property int senseHeight: units.gu(6)
			readonly property int xCenter: cellWidth / 2
			readonly property int yCenter: cellHeight / 2
			readonly property int senseXHalf: senseWidth / 2
			readonly property int senseYHalf: senseHeight / 2
			readonly property int senseXMin: xCenter - senseXHalf
			readonly property int senseXMax: xCenter + senseXHalf
			readonly property int senseYMin: yCenter - senseYHalf
			readonly property int senseYMax: yCenter + senseYHalf
			function cellIndex(mouse) {
				var gridX = mouse.x - x
				var gridY = mouse.y - y
				var xOffset = gridX % cellWidth
				if (xOffset < senseXMin || xOffset > senseXMax) {
					return undefined
				}
				var yOffset = gridY % cellHeight
				if (yOffset < senseYMin || yOffset > senseYMax) {
					return undefined
				}
				return (
					Math.floor(gridY / cellHeight) * board.size +
					Math.floor(gridX / cellWidth)
				)
			}
			onPressed: {
				var index = cellIndex(mouse)
				if (index !== undefined) {
					yourWords.startWord(board.get(index).letter)
					board.selectFirst(index)
				}
			}
			onPositionChanged: {
				if (board.firstSelection !== undefined) {
					var index = cellIndex(mouse)
					if (index !== undefined) {
						var cube = board.get(index)
						if (
							!cube.selected &&
							index in board.lastAdjacencies
						) {
							yourWords.extendWord(cube.letter)
							board.select(index)
						}
					}
				}
			}
			onReleased: {
				if (board.firstSelection !== undefined)
					board.selectedWord()
			}
		}
	}
}

