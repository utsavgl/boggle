// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

Item {

	// required
	property string title

	// required
	property WordList wordList

	// optional
	property bool incremental: false

	readonly property int margin: units.gu(0.4)

	anchors {
		top: parent.top
		bottom: parent.bottom
	}
	width: childrenRect.width

	RowLayout {
		id: total
		anchors {
			top: parent.top
			left: parent.left
			leftMargin: margin
			rightMargin: margin
			right: parent.right
		}
		height: childrenRect.height
		Label {
			text: title
			textSize: Label.Large
			font.italic: true
		}
		Label {
			Layout.alignment: Qt.AlignRight
			text: (
				incremental && board.showingCubes || !hourglass.hasTimeLeft ?
				wordList.score :
				""
			)
			textSize: Label.Large
			font.bold: true
		}
	}

	Rectangle {
		anchors {
			top: total.bottom
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}
		border {
			width: margin
			color: theme.palette.normal.overlayText
		}
		color: theme.palette.normal.foreground
		ListView {
			id: listView
			anchors {
				fill: parent
				margins: margin
			}
			model: wordList
			clip: true
			delegate: Rectangle {
				width: listView.width
				height: childrenRect.height
				color: (
					inProgress ? (
						duplicate ?
						theme.palette.normal.negative :
						theme.palette.normal.selection
					) :
					theme.palette.normal.foreground
				)
				ListView.onAdd: {
					if (incremental) {
						listView.currentIndex = index
					}
				}
				RowLayout {
					anchors {
						left: parent.left
						leftMargin: margin
						rightMargin: margin
						right: parent.right
					}
					Label {
						text: word
						textSize: Label.Large
						font.strikeout: inCommon
					}
					Label {
						Layout.alignment: Qt.AlignRight
						text: inProgress ? "" : score
						textSize: Label.Medium
					}
				}
			}
		}
	}
}

