// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12

ListModel {

// External

	function reset() {
		clear()
		set = {}
		score = 0
	}

	function has(word) {
		return word in set
	}

	function cancelCommon(otherWordList) {
		for (var i = 0; i < count; ++i) {
			if (otherWordList.has(get(i).word)) {
				setProperty(i, "inCommon", true)
			}
		}
	}

	property int score: 0

// Internal

	property var set: ({})

	function update(word, thisScore) {
		set[word] = true
		score += thisScore
	}

	function wordScore(word) {
		switch (word.length) {
			case 3:
				return 1
			case 4:
				return 1
			case 5:
				return 2
			case 6:
				return 3
			case 7:
				return 5
			default:
				return 11
		}
	}
}

