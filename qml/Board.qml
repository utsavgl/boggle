// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12

ListModel {

	readonly property int size: 4

// External

	function shake() {
		shuffle(cubes)
		for (var i = 0; i < n; ++i) {
			var j = Math.floor(Math.random() * 6)
			var letter = letters[cubes[i]].substr(j, 1)
			setProperty(i, "letter", letter === "Q" ? "QU" : letter)
		}
	}

	property bool showingCubes: count > 0 && get(0).letter !== ""

	function findWords(wordList) {
		for (var i = 0; i < n; ++i) {
			visibleLetters[i] = get(i).letter
			used[i] = false
		}
		for (var i = 0; i < n; ++i) {
			exploreWords(i, dictionary.trie, wordList)
		}
	}

	property var firstSelection: undefined
	property var lastSelection: undefined
	property bool wordSelected: false

	property var lastAdjacencies: (
		lastSelection !== undefined ? adjacencies[lastSelection] : undefined
	)

	function selectFirst(index) {
		firstSelection = index
		select(index)
	}

	function clear() {
		for (var i = 0; i < n; ++i) {
			setProperty(i, "selected", false)
		}
		firstSelection = undefined
		lastSelection = undefined
		wordSelected = false
	}

	function select(index) {
		setProperty(index, "selected", true)
		lastSelection = index
	}

	function selectedWord() {
		wordSelected = true
	}

	function reset() {
		for (var i = 0; i < n; ++i) {
			setProperty(i, "letter", "")
			setProperty(i, "selected", false)
		}
	}

// Internal

	readonly property int n: size * size
	readonly property var letters: [
		"MORAHS",
		"OFIRBX",
		"RELASC",
		"DENAZV",
		"VETIGN",
		"OQAMBJ",
		"IFEHYE",
		"YLTBIA",
		"PTLSEU",
		"DUNOTK",
		"ESOWDN",
		"MADEPC",
		"PINEHS",
		"EYUGLK",
		"AIOATC",
		"IWULRG"
	]
	property var cubes: []

	function initialize() {
		for (var i = 0; i < n; ++i) {
			append({letter: "", selected: false})
			cubes[i] = i
		}
	}

	function shuffle(a) {
		for (var i = 0; i < n - 1; ++i) {
			var r = Math.floor(Math.random() * (n - i))
			var j = i + r
			var tmp = a[j]
			a[j] = a[i]
			a[i] = tmp
		}
	}

	property var visibleLetters: new Array(n)
	property var used: new Array(n)

	function exploreWords(i, supertrie, wordList) {
		var letter = visibleLetters[i]
		var trie = dictionary.subtrie(letter[0], supertrie)
		if (letter === "QU" && trie !== undefined) {
			trie = dictionary.subtrie(letter[1], trie)
		}
		if (trie !== undefined) {
			var word = dictionary.word(trie)
			if (word !== undefined) {
				if (!wordList.has(word)) {
					wordList.add(word)
				}
			}
			used[i] = true
			for (var j in adjacencies[i]) {
				if (!used[j]) {
					exploreWords(j, trie, wordList)
				}
			}
			used[i] = false
		}
	}

	property var adjacencies: []

	function computeAdjacencies() {
		for (var y = 0; y < size; ++y) {
			for (var x = 0; x < size; ++x) {
				var adjs = {}
				for (var dy = -1; dy <= 1; ++dy) {
					for (var dx = -1; dx <= 1; ++dx) {
						if (!(dy === 0 && dx === 0)) {
							var ay = y + dy
							if (ay >= 0 && ay < size) {
								var ax = x + dx
								if (ax >= 0 && ax < size) {
									adjs[ay * size + ax] = true
								}
							}
						}
					}
				}
				adjacencies.push(adjs)
			}
		}
	}

	Component.onCompleted: {
		initialize()
		computeAdjacencies()
	}
}

