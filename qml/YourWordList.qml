// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12

WordList {

// External

	property bool wordEndable: (
		lastWord !== undefined && lastWord.length >= 3 && !has(lastWord)
	)

	function startWord(letter) {
		append({
			word: letter,
			inProgress: true,
			duplicate: false,
			inCommon: false,
			score: 0
		})
	}

	function clearWord() {
		remove(lastIndex)
	}

	function endWord() {
		var lastWordScore = wordScore(lastWord)
		setProperty(lastIndex, "inProgress", false)
		setProperty(lastIndex, "score", lastWordScore)
		update(lastWord, lastWordScore)
	}

	function extendWord(letter) {
		var extendedWord = lastWord + letter
		setProperty(lastIndex, "word", extendedWord)
		setProperty(lastIndex, "duplicate", has(extendedWord))
	}

// Internal

	property var lastIndex: count > 0 ? count - 1 : undefined
	property var last: lastIndex !== undefined ? get(lastIndex) : undefined
	property var lastWord: last !== undefined ? last.word : undefined
}

