// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12

Timer {
	interval: 1000
	repeat: true
	onTriggered: {
		--secondsLeft
		if (secondsLeft === 0) {
			stop()
		}
	}

// External

	function reset() {
		secondsLeft = gameSeconds
	}

	function empty() {
		if (running) {
			stop()
			if (secondsLeft !== 0) {
				secondsLeft = 0
			}
		}
	}

	property bool hasTimeLeft: secondsLeft > 0
	property string formattedTime: (
		minutes + ":" + (seconds < 10 ? "0" + seconds : seconds)
	)

// Internal

	readonly property int gameSeconds: 180
	property int secondsLeft: gameSeconds
	property int minutes: Math.floor(secondsLeft / 60)
	property int seconds: secondsLeft % 60
}

