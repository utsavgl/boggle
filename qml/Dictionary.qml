// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12

QtObject {
	id: dictionary

// External

	function has(candidate) {
		var trie = dictionary.trie
		for (var i = 0; i < candidate.length; ++i) {
			trie = subtrie(candidate[i], trie)
			if (trie === undefined) {
				return false
			}
		}
		return word(trie) !== undefined
	}

	function subtrie(letter, trie) {
		return trie[letter]
	}

	function word(trie) {
		return trie.word
	}

	property var trie: ({})

// Internal

	readonly property Words wordsObject: Words {}
	readonly property var words: wordsObject.all

	function populate(word, subtrie) {
		for (var i = 0; i < word.length; ++i) {
			var letter = word[i]
			if (!(letter in subtrie)) {
				subtrie[letter] = {}
			}
			subtrie = subtrie[letter]
		}
		subtrie.word = word
	}

	Component.onCompleted: {
		for (var i = 0; i < words.length; ++i) {
			populate(words[i], trie)
		}
	}
}

