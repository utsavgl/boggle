// Copyright 2019 Utsav

// This file is part of Boggle.

// Boggle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Boggle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Boggle.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick 2.12

WordList {

// External

	function add(word) {
		var thisScore = wordScore(word)
		append({
			word: word,
			inProgress: false,
			duplicate: false,
			inCommon: false,
			score: thisScore
		})
		update(word, thisScore)
	}
}

